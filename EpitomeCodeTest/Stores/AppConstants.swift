//
//  AppConstants.swift
//  EpitomeCodeTest
//
//  Created by Lakhwinder Singh on 15/01/18.
//  Copyright © 2018 LakhwinderSingh. All rights reserved.
//

let BASE_URL = "https://jsonplaceholder.typicode.com/"

let API_USER = "\(BASE_URL)users"
let API_POST = "\(BASE_URL)posts?userId=%i"
let API_ALBUM = "\(BASE_URL)albums?userId=%i"
let API_TODO = "\(BASE_URL)todos?userId=%i"
let API_PHOTO = "\(BASE_URL)photos?albumId=%i"




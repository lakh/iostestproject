//
//  TodoListControl.swift
//  EpitomeCodeTest
//
//  Created by Lakhwinder Singh on 16/01/18.
//  Copyright © 2018 LakhwinderSingh. All rights reserved.
//

import UIKit

class TodoListControl: UITableViewController {

    // MARK:- Constants and stored variables to used commonly
    var mainControl: HomeControl!
    var selectedUser: User!
    var items: [Todo]! = []
    let cellClass = TodoListCell.self
    var reuseId: String {
        return self.cellClass.reuseId
    }
    
    // MARK: View load functions
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        tableView?.rowHeight = UITableViewAutomaticDimension
        tableView.register(cellClass, forCellReuseIdentifier: reuseId)
        tableView.isScrollEnabled = false
        
        requestItems()
    }
    
    // MARK: Request items from server
    func requestItems() {
        if selectedUser != nil {
            APIStore.shared.requestTodo(selectedUser, { (todos, success) in
                if success {
                    self.items = todos
                } else {
                    self.items.removeAll()
                }
                self.tableView?.reloadData()
            })
        }
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseId, for: indexPath) as! TodoListCell
        cell.updateWithModel(self.items[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK:- Layout subviews functions
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        mainControl.todosHieghtConstraint.constant = tableView.contentSize.height
        mainControl.view.layoutIfNeeded()
    }
    
}



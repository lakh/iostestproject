//
//  PostListCell.swift
//  EpitomeCodeTest
//
//  Created by Lakhwinder Singh on 15/01/18.
//  Copyright © 2018 LakhwinderSingh. All rights reserved.
//

import UIKit

class PostListCell: UITableViewCell {

    // MARK:- Class variables to access in list control
    class var reuseId: String {
        return String(describing: self)
    }
    
    // MARK:- Initialise functions
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initViews()
    }
    
    func initViews() {
        backgroundColor = .clear
        addSubview(mainContainer)
        mainContainer.addSubview(titleLabel)
        layoutIfNeeded()
    }
    
    // MARK:- Update model
    func updateWithModel(_ model: Any) {
        let post = model as! Post
        titleLabel.text = post.title
    }
    
    // MARK:- Setter getter variables
    let mainContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.numberOfLines = 0
        return label
    }()
    
    // MARK:- Layout subviews
    override func layoutSubviews() {
        super.layoutSubviews()
        addVisualConstraints(["H:|[main]|", "V:|-2-[main]-2-|"], subviews: ["main": mainContainer])
        mainContainer.addVisualConstraints(["H:|-5-[title]-5-|", "V:|-5-[title]-5-|"], subviews: ["title": titleLabel])
    }

}



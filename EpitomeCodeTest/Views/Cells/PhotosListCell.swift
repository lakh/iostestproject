//
//  PhotosListCell.swift
//  EpitomeCodeTest
//
//  Created by Lakhwinder Singh on 16/01/18.
//  Copyright © 2018 LakhwinderSingh. All rights reserved.
//

import UIKit

/*
 All functions handle through Album list cell.
 Any additional work can be done in this class
 */
class PhotosListCell: AlbumListCell {
    
    override func updateWithModel(_ model: Any) {
        let photo = model as! Photo
        titleLabel.text = photo.title
    }
    
}




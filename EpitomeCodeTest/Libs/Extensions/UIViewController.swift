//
//  UIViewController.swift
//
//  Created by Lakhwinder Singh on 23/02/17.
//  Copyright © 2017 lakh. All rights reserved.
//

import UIKit

extension UIViewController {
    
    class var identifier: String {
        return String(describing: self)
    }
    
    func customAddChildViewController(_ child: UIViewController) {
        self.customAddChildViewController(child, toSubview: self.view)
    }
    
    func customAddChildViewController(_ child: UIViewController, toSubview subview: UIView) {
        self.addChildViewController(child)
        subview.addSubview(child.view)
        child.view.addConstraintToFillSuperview()
        child.didMove(toParentViewController: self)
    }
    
    func customRemoveFromParentViewController() {
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    func customRemoveAllChildViewControllers() {
        for control: UIViewController in self.childViewControllers {
            control.customRemoveFromParentViewController()
        }
    }

}



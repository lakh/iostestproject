//
//  AlbumListControl.swift
//  EpitomeCodeTest
//
//  Created by Lakhwinder Singh on 15/01/18.
//  Copyright © 2018 LakhwinderSingh. All rights reserved.
//

import UIKit

let ALBUM_LIST_CELL_SIZE: CGFloat = 130

class AlbumListControl: UICollectionViewController {
    
    // MARK:- Constants and stored variables to used commonly
    var mainControl: HomeControl!
    var selectedUser: User!
    var items: [Album]! = []
    let cellClass = AlbumListCell.self
    var reuseId: String {
        return self.cellClass.reuseId
    }
    
    // MARK: View load functions
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register cell classes
        collectionView?.backgroundColor = .clear
        collectionView!.showsVerticalScrollIndicator = false
        collectionView!.showsHorizontalScrollIndicator = false
        collectionView!.alwaysBounceVertical = false
        collectionView!.register(cellClass, forCellWithReuseIdentifier: reuseId)
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize.init(width: ALBUM_LIST_CELL_SIZE*1.3, height: ALBUM_LIST_CELL_SIZE)
        layout.scrollDirection = .horizontal
        layout.sectionInset = .zero
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        collectionView?.setCollectionViewLayout(layout, animated: true)
        
        requestItems()
    }
    
    // MARK: Request items from server
    func requestItems() {
        if selectedUser != nil {
            APIStore.shared.requestAlbum(selectedUser, { (albums, success) in
                if success {
                    self.items = albums
                } else {
                    self.items.removeAll()
                }
                self.collectionView?.reloadData()
            })
        }
    }
    
    // MARK: UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath) as! AlbumListCell
        cell.updateWithModel(items[indexPath.row])
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        mainControl.navigationController?.pushViewController(AlbumDetailControl.controlWithModel(items[indexPath.row]), animated: true)
    }

}



//
//  DetailControl.swift
//  EpitomeCodeTest
//
//  Created by Lakhwinder Singh on 15/01/18.
//  Copyright © 2018 LakhwinderSingh. All rights reserved.
//

import UIKit

class PostDetailControl: UIViewController {

    // MARK:- Stored and outlet variables
    var post: Post!
    @IBOutlet weak var bodyTextView: UITextView!

    // MARK:- Class function with variable
    class func controlWithModel(_ post: Post) -> PostDetailControl {
        let control = UIStoryboard.main.instantiateViewController(withIdentifier: self.identifier) as! PostDetailControl
        control.post = post
        return control
    }
    
    // MARK:- View load functions
    override func viewDidLoad() {
        super.viewDidLoad()
        showPostDetail()
    }
    
    func showPostDetail() {
        title = post.title
        bodyTextView.text = post.body
    }
}



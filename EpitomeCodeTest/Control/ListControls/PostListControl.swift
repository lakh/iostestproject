//
//  PostListControl.swift
//  EpitomeCodeTest
//
//  Created by Lakhwinder Singh on 15/01/18.
//  Copyright © 2018 LakhwinderSingh. All rights reserved.
//

import UIKit

class PostListControl: UITableViewController {

    // MARK:- Constants and stored variables to used commonly
    var mainControl: HomeControl!
    var selectedUser: User!
    var items: [Post]! = []
    let cellClass = PostListCell.self
    var reuseId: String {
        return self.cellClass.reuseId
    }

    // MARK: View load functions
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        tableView?.rowHeight = UITableViewAutomaticDimension
        tableView.register(cellClass, forCellReuseIdentifier: reuseId)
        tableView.isScrollEnabled = false
        
        requestItems()
    }
    
    // MARK: Request items from server
    func requestItems() {
        if selectedUser != nil {
            APIStore.shared.requestPost(selectedUser, { (posts, success) in
                if success {
                    self.items = posts
                } else {
                    self.items.removeAll()
                }
                self.tableView?.reloadData()
            })
        }
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseId, for: indexPath) as! PostListCell
        cell.updateWithModel(self.items[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        mainControl.navigationController?.pushViewController(PostDetailControl.controlWithModel(items[indexPath.row]), animated: true)
    }
    
    // MARK:- Layout subviews functions
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        mainControl.postHieghtConstraint.constant = tableView.contentSize.height
        mainControl.view.layoutIfNeeded()
    }
    
}



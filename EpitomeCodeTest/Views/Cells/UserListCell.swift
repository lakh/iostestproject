//
//  UserListCell.swift
//  EpitomeCodeTest
//
//  Created by Lakhwinder Singh on 15/01/18.
//  Copyright © 2018 LakhwinderSingh. All rights reserved.
//

import UIKit

class UserListCell: UICollectionViewCell {
    
    // MARK:- Class variables to access in list control
    class var reuseId: String {
        return String(describing: self)
    }
    
    // MARK:- Initialise functions
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initViews()
    }
    
    func initViews() {
        backgroundColor = .clear
        addSubview(imageView)
        layoutIfNeeded()
    }
    
    // MARK:- Setter getter variables
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = #imageLiteral(resourceName: "user_placeholder")
        return imageView
    }()
    
    // MARK:- Layout subviews
    override func layoutSubviews() {
        super.layoutSubviews()
        addVisualConstraints(["H:|-5-[image]-5-|", "V:|-5-[image]-5-|"], subviews: ["image": imageView])
        imageView.layer.cornerRadius = imageView.height/2
    }
    
}



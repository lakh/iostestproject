//
//  PhotoListControl.swift
//  EpitomeCodeTest
//
//  Created by Lakhwinder Singh on 16/01/18.
//  Copyright © 2018 LakhwinderSingh. All rights reserved.
//

import UIKit

class PhotoListControl: UICollectionViewController {
    
    // MARK:- Constants and stored variables to used commonly
    var selectedAlbum: Album!
    var items: [Photo]! = []
    let cellClass = PhotosListCell.self
    var reuseId: String {
        return self.cellClass.reuseId
    }
    
    // MARK: View load functions
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register cell classes
        collectionView?.backgroundColor = .clear
        collectionView!.showsVerticalScrollIndicator = false
        collectionView!.showsHorizontalScrollIndicator = false
        collectionView!.alwaysBounceVertical = true
        collectionView!.register(cellClass, forCellWithReuseIdentifier: reuseId)
        
        let layout = UICollectionViewFlowLayout()
        let cellWidth = view.width/2
        layout.itemSize = CGSize.init(width: cellWidth, height: cellWidth * 0.7)
        layout.scrollDirection = .vertical
        layout.sectionInset = .zero
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        collectionView?.setCollectionViewLayout(layout, animated: true)
        
        requestItems()
    }
    
    // MARK: Request items from server
    func requestItems() {
        if selectedAlbum != nil {
            APIStore.shared.requestPhoto(selectedAlbum, { (photos, success) in
                if success {
                    self.items = photos
                } else {
                    self.items.removeAll()
                }
                self.collectionView?.reloadData()
            })
        }
    }
    
    // MARK: UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath) as! PhotosListCell
        cell.updateWithModel(items[indexPath.row])
        return cell
    }

}



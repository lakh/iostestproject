//
//  ViewController.swift
//  EpitomeCodeTest
//
//  Created by Lakhwinder Singh on 15/01/18.
//  Copyright © 2018 LakhwinderSingh. All rights reserved.
//

import UIKit

class HomeControl: UIViewController {

    // MARK:- Outlet variables
    @IBOutlet weak var selectUserLabel: UILabel!
    
    //Outelt variables for parent views.
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var postView: UIView!
    @IBOutlet weak var albumView: UIView!
    @IBOutlet weak var todoView: UIView!
    
    //Outelt variables for all list container
    @IBOutlet weak var userListContainer: UIView!
    @IBOutlet weak var postListContainer: UIView!
    @IBOutlet weak var albumListContainer: UIView!
    @IBOutlet weak var todoListContainer: UIView!
    
    //Outelt variables to manage list hieght
    @IBOutlet weak var userHieghtConstraint: NSLayoutConstraint!
    @IBOutlet weak var postHieghtConstraint: NSLayoutConstraint!
    @IBOutlet weak var albumsHieghtConstraint: NSLayoutConstraint!
    @IBOutlet weak var todosHieghtConstraint: NSLayoutConstraint!
    
    // MARK:- View load functions
    override func viewDidLoad() {
        super.viewDidLoad()
        addListControls()
    }
    
    // MARK:- Add list controls
    func addListControls() {
        customAddChildViewController(userListControl, toSubview: userListContainer)
        customAddChildViewController(postListControl, toSubview: postListContainer)
        customAddChildViewController(albumListControl, toSubview: albumListContainer)
        customAddChildViewController(todoListControl, toSubview: todoListContainer)
    }
    
    // MARK:- Stores variables for list controls
    private var _userListControl: UserListControl!
    private var _postListControl: PostListControl!
    private var _albumListControl: AlbumListControl!
    private var _todoListControl: TodoListControl!
    
    // MARK:- Update functions to call from list controls
    func selectUser(_ user: User) {
        selectUserLabel.isHidden = true
        postView.isHidden = false
        albumView.isHidden = false
        todoView.isHidden = false
        
        postListControl.selectedUser = user
        postListControl.requestItems()
        
        albumListControl.selectedUser = user
        albumListControl.requestItems()
        
        todoListControl.selectedUser = user
        todoListControl.requestItems()
    }
    
    // MARK:- Layout subviews.
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        userHieghtConstraint.constant = USER_LIST_CELL_SIZE
        albumsHieghtConstraint.constant = ALBUM_LIST_CELL_SIZE
    }

}

//////////////////////////////////////////////////////////////////////////////////////////

/*
 Extension for getter variables
 */
extension HomeControl {
    
    // MARK:- Getters for list controls
    var userListControl: UserListControl {
        if _userListControl == nil {
            _userListControl = UserListControl.init(collectionViewLayout: UICollectionViewLayout())
            _userListControl.mainControl = self
        }
        return _userListControl
    }
    
    var postListControl: PostListControl {
        if _postListControl == nil {
            _postListControl = PostListControl.init(style: .plain)
            _postListControl.mainControl = self
        }
        return _postListControl
    }
    
    var albumListControl: AlbumListControl {
        if _albumListControl == nil {
            _albumListControl = AlbumListControl.init(collectionViewLayout: UICollectionViewLayout())
            _albumListControl.mainControl = self
        }
        return _albumListControl
    }

    var todoListControl: TodoListControl {
        if _todoListControl == nil {
            _todoListControl = TodoListControl.init(style: .plain)
            _todoListControl.mainControl = self
        }
        return _todoListControl
    }

}



//
//  APIStore.swift
//  EpitomeCodeTest
//
//  Created by Lakhwinder Singh on 15/01/18.
//  Copyright © 2018 LakhwinderSingh. All rights reserved.
//

import UIKit

class APIStore: NSObject {

    static let shared = APIStore()
    
    // MARK:- Common function to call api and get response in anyobject.
    private func requestAPI(_ url: String, _ completion: @escaping (_ : AnyObject?) -> Void) {
        URLSession.shared.dataTask(with: URL(string: url)!) { (data, response, error) in
            DispatchQueue.main.async {
                if data != nil {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                        completion(json as AnyObject)
                    } catch _ as NSError {
                        completion(nil)
                    }
                } else {
                    completion(nil)
                }
            }
        }.resume()
    }

    // MARK:- Function to get list of all users.
    func requestUsers(_ completion: @escaping (_ : [User]?, _ : Bool) -> Void) {
        requestAPI(API_USER) { (object) in
            if object != nil {
                var items: [User] = []
                for dict in object as! NSArray {
                    items.append(User.init(object: dict))
                }
                completion(items, true)
            } else {
                completion(nil, false)
            }
        }
    }
    
    // MARK:- Function to get post based on user
    func requestPost(_ user: User, _ completion: @escaping (_ : [Post]?, _ : Bool) -> Void) {
        requestAPI(String(format: API_POST, user.id!)) { (object) in
            if object != nil {
                var items: [Post] = []
                for dict in object as! NSArray {
                    items.append(Post.init(object: dict))
                }
                completion(items, true)
            } else {
                completion(nil, false)
            }
        }
    }
    
    // MARK:- Function to get albums based on user
    func requestAlbum(_ user: User, _ completion: @escaping (_ : [Album]?, _ : Bool) -> Void) {
        requestAPI(String(format: API_ALBUM, user.id!)) { (object) in
            if object != nil {
                var items: [Album] = []
                for dict in object as! NSArray {
                    items.append(Album.init(object: dict))
                }
                completion(items, true)
            } else {
                completion(nil, false)
            }
        }
    }

    // MARK:- Function to get todo based on user
    func requestTodo(_ user: User, _ completion: @escaping (_ : [Todo]?, _ : Bool) -> Void) {
        requestAPI(String(format: API_TODO, user.id!)) { (object) in
            if object != nil {
                var items: [Todo] = []
                for dict in object as! NSArray {
                    items.append(Todo.init(object: dict))
                }
                completion(items, true)
            } else {
                completion(nil, false)
            }
        }
    }
    
    // MARK:- Function to get photos based on user
    func requestPhoto(_ album: Album, _ completion: @escaping (_ : [Photo]?, _ : Bool) -> Void) {
        requestAPI(String(format: API_PHOTO, album.id!)) { (object) in
            if object != nil {
                var items: [Photo] = []
                for dict in object as! NSArray {
                    items.append(Photo.init(object: dict))
                }
                completion(items, true)
            } else {
                completion(nil, false)
            }
        }
    }
    
}



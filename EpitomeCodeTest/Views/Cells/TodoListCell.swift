//
//  TodoListCell.swift
//  EpitomeCodeTest
//
//  Created by Lakhwinder Singh on 16/01/18.
//  Copyright © 2018 LakhwinderSingh. All rights reserved.
//

import UIKit

/*
 All functions handle through post list cell.
 Any additional work can be done in this class
 */
class TodoListCell: PostListCell {
        
    // MARK:- Update model
    override func updateWithModel(_ model: Any) {
        let todo = model as! Todo
        titleLabel.text = todo.title
    }
}



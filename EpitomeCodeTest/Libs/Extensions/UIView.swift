//
//  UIView.swift
//  MMA App
//
//  Created by Lakhwinder Singh on 31/03/17.
//  Copyright © 2017 lakh. All rights reserved.
//

import UIKit

extension UIView {
    
    var height: CGFloat {
        get {
            return bounds.size.height
        }
        set {
            frame.size.height = newValue
        }
    }
    
    var width: CGFloat {
        get {
            return bounds.size.width
        }
        set {
            frame.size.width = newValue
        }
    }

    var contentCompressionResistancePriority: UILayoutPriority {
        get {
            let horizontal: UILayoutPriority = contentCompressionResistancePriority(for: .horizontal)
            let vertical: UILayoutPriority = contentCompressionResistancePriority(for: .vertical)
            return UILayoutPriority(rawValue: (horizontal.rawValue + vertical.rawValue) * 0.5)
        }
        set {
            setContentCompressionResistancePriority(newValue, for: .horizontal)
            setContentCompressionResistancePriority(newValue, for: .vertical)
        }
    }
    
    var contentHuggingPriority: UILayoutPriority {
        get {
            let horizontal: UILayoutPriority = contentHuggingPriority(for: .horizontal)
            let vertical: UILayoutPriority = contentHuggingPriority(for: .vertical)
            return UILayoutPriority(rawValue: (horizontal.rawValue + vertical.rawValue) * 0.5)
        }
        set {
            setContentHuggingPriority(newValue, for: .horizontal)
            setContentHuggingPriority(newValue, for: .vertical)
        }
    }
    
    func bringSubviewToFront(_ subview: UIView, withSuperviews number: Int) {
        var subview = subview
        for _ in 0...number {
            subview.superview?.bringSubview(toFront: subview)
            subview = subview.superview!
        }
    }
    
    func addConstraint(_ view1: UIView, view2: UIView, att1: NSLayoutAttribute, att2: NSLayoutAttribute, mul: CGFloat, const: CGFloat) -> NSLayoutConstraint {
        if view2.responds(to: #selector(setter: self.translatesAutoresizingMaskIntoConstraints)) {
            view2.translatesAutoresizingMaskIntoConstraints = false
        }
        let constraint = NSLayoutConstraint(item: view1, attribute: att1, relatedBy: .equal, toItem: view2, attribute: att2, multiplier: mul, constant: const)
        addConstraint(constraint)
        return constraint
    }
    
    func addConstraint(_ view: UIView, att1: NSLayoutAttribute, att2: NSLayoutAttribute, mul: CGFloat, const: CGFloat) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: att1, relatedBy: .equal, toItem: view, attribute: att2, multiplier: mul, constant: const)
        addConstraint(constraint)
        return constraint
    }
    
    func addConstraintSameCenterX(_ view1: UIView, view2: UIView, mul: CGFloat = 1.0, const: CGFloat = 0.0) {
        _ = addConstraint(view1, view2: view2, att1: .centerX, att2: .centerX, mul: mul, const: const)
    }
    
    func addConstraintSameCenterY(_ view1: UIView, view2: UIView, mul: CGFloat = 1.0, const: CGFloat = 0.0) {
        _ = addConstraint(view1, view2: view2, att1: .centerY, att2: .centerY, mul: mul, const: const)
    }
    
    func addConstraintSameHeight(_ view1: UIView, view2: UIView, mul: CGFloat = 1.0, const: CGFloat = 0.0) {
        _ = addConstraint(view1, view2: view2, att1: .height, att2: .height, mul: mul, const: const)
    }
    
    func addConstraintSameWidth(_ view1: UIView, view2: UIView, mul: CGFloat = 1.0, const: CGFloat = 0.0) {
        _ = addConstraint(view1, view2: view2, att1: .width, att2: .width, mul: mul, const: const)
    }
    
    func addConstraintSameCenterXY(_ view1: UIView, view2: UIView) {
        _ = addConstraintSameCenterX(view1, view2: view2)
        _ = addConstraintSameCenterY(view1, view2: view2)
    }
    
    func addConstraintSameSize(_ view1: UIView, view2: UIView) {
        _ = addConstraintSameWidth(view1, view2: view2)
        _ = addConstraintSameHeight(view1, view2: view2)
    }
    
    func addConstraintSameAttribute(_ attribute: NSLayoutAttribute, subviews: [UIView]) {
        for i in 1..<subviews.count {
            addConstraint(NSLayoutConstraint(item: subviews[0], attribute: attribute, relatedBy: .equal, toItem: subviews[i], attribute: attribute, multiplier: 1.0, constant: 0.0))
        }
    }
    
    func addVisualConstraints(_ constraints: [String], metrics: [String: Any]? = nil, subviews: [String: UIView]) {
        // Disable autoresizing masks translation for all subviews
        for subview in subviews.values {
            if subview.responds(to: #selector(setter: self.translatesAutoresizingMaskIntoConstraints)) {
                subview.translatesAutoresizingMaskIntoConstraints = false
            }
        }
        // Apply all constraints
        for constraint in constraints {
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: constraint, options: [], metrics: metrics, views: subviews))
        }
    }
    
    func addConstraintToFillSuperview() {
        superview?.addVisualConstraints(["H:|[self]|", "V:|[self]|"], subviews: ["self": self])
    }
    
    func removeConstraints() {
        var superview: UIView? = self.superview
        while superview != nil {
            for c: NSLayoutConstraint in (superview?.constraints)! {
                if c.firstItem as! NSObject == self || (c.secondItem != nil && c.secondItem as! NSObject == self) {
                    superview?.removeConstraint(c)
                }
            }
            superview = superview?.superview
        }
    }

    func addConstraintForAspectRatio(_ aspectRatio: CGFloat) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: self, attribute: .height, multiplier: aspectRatio, constant: 0.0)
        addConstraint(constraint)
        return constraint
    }
    
    func addConstraintForWidth(_ width: CGFloat) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0.0, constant: width)
        addConstraint(constraint)
        return constraint
    }
    
    func addConstraintForHeight(_ height: CGFloat) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0.0, constant: height)
        addConstraint(constraint)
        return constraint
    }
    
    func addSubviews(_ subviews: [UIView]) {
        for view in subviews {
            addSubview(view)
        }
    }
}



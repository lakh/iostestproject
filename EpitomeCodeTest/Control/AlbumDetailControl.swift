//
//  AlbumControl.swift
//  EpitomeCodeTest
//
//  Created by Lakhwinder Singh on 16/01/18.
//  Copyright © 2018 LakhwinderSingh. All rights reserved.
//

import UIKit

class AlbumDetailControl: UIViewController {

    // MARK:- Outlet variables
    var album: Album!
    @IBOutlet weak var photoListContainer: UIView!
    
    // MARK:- Class function with variable
    class func controlWithModel(_ album: Album) -> AlbumDetailControl {
        let control = UIStoryboard.main.instantiateViewController(withIdentifier: self.identifier) as! AlbumDetailControl
        control.album = album
        return control
    }
    
    // MARK:- View load functions
    override func viewDidLoad() {
        super.viewDidLoad()
        addListControls()
        showPhotos()
    }
    
    // MARK:- Add list controls
    func addListControls() {
        customAddChildViewController(photoListControl, toSubview: photoListContainer)
    }
    
    // MARK:- Pass select album to photo list control
    func showPhotos() {
        title = album.title
        photoListControl.selectedAlbum = album
        photoListControl.requestItems()
    }
    
    // MARK:- Constant variables
    let photoListControl = PhotoListControl.init(collectionViewLayout: UICollectionViewLayout())
    
    // MARK:- Layout subviews.
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
}



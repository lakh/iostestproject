//
//  AlbumListCell.swift
//  EpitomeCodeTest
//
//  Created by Lakhwinder Singh on 16/01/18.
//  Copyright © 2018 LakhwinderSingh. All rights reserved.
//

import UIKit

class AlbumListCell: UICollectionViewCell {
    
    // MARK:- Class variables to access in list control
    class var reuseId: String {
        return String(describing: self)
    }
    
    // MARK:- Initialise functions
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initViews()
    }
    
    func initViews() {
        backgroundColor = .clear
        addSubviews([imageView, titleLabel])
        layoutIfNeeded()
    }
    
    // MARK:- Update model
    func updateWithModel(_ model: Any) {
        let album = model as! Album
        titleLabel.text = album.title
    }
    
    // MARK:- Setter getter variables
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .black
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 12.0)
        return label
    }()
    
    // MARK:- Layout subviews
    override func layoutSubviews() {
        super.layoutSubviews()
        addVisualConstraints(["H:|-5-[image]-5-|", "V:|-5-[image]-5-|", "H:|-5-[title]-5-|", "V:[title]-5-|"], subviews: ["image": imageView, "title": titleLabel])
    }
    
}



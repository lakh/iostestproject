
Objective:
Create a test iOS app that allows user to view any user's posts, albums & todos.

APIs:
Base Url: https://jsonplaceholder.typicode.com

Endpoints:
users
posts?userId={userId}
albums
todos
photos

Requirements:
Home initially when no user is selected, the table should show empty state with message "Please select a user�. When selected, his posts/albums/todos are displayed.
Post on selection should show title and body on next screen.
Album on selection should show photos of that album on next screen.
Todo when clicked should update their �completed� status.

Note:
Please refer to the attached screenshot for design. Also commit the updates to a Git repository module wise.
	
Submission time:
2 days (until 17th Jan 2018 09:00am)

Do not use any boilerplate code. Keep the sample to-the-point.
Once done, email us the public link to your repository so we can take a look at the code and revert back to you about it ASAP.

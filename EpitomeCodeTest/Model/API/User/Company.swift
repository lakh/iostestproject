//
//  Company.swift
//
//  Created by Lakhwinder Singh on 15/01/18
//  Copyright (c) . All rights reserved.
//

import Foundation

public final class Company: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let bs = "bs"
        static let name = "name"
        static let catchPhrase = "catchPhrase"
    }
    
    // MARK: Properties
    public var bs: String?
    public var name: String?
    public var catchPhrase: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        bs = json[SerializationKeys.bs].string
        name = json[SerializationKeys.name].string
        catchPhrase = json[SerializationKeys.catchPhrase].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = bs { dictionary[SerializationKeys.bs] = value }
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = catchPhrase { dictionary[SerializationKeys.catchPhrase] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.bs = aDecoder.decodeObject(forKey: SerializationKeys.bs) as? String
        self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
        self.catchPhrase = aDecoder.decodeObject(forKey: SerializationKeys.catchPhrase) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(bs, forKey: SerializationKeys.bs)
        aCoder.encode(name, forKey: SerializationKeys.name)
        aCoder.encode(catchPhrase, forKey: SerializationKeys.catchPhrase)
    }
    
}


